APP_NAME = omniview

VERSION:=$(shell cat VERSION)
ifeq ($(CI_COMMIT_SHORT_SHA),)
	CI_COMMIT_SHORT_SHA:=$(shell git rev-parse --short HEAD)
endif
COMPILE_DATE:=$(shell LC_TIME=C date +"%F %T %z")
SHORT_DATE:=$(shell LC_TIME=C date +"%Y%m%d")

DEB_VERSION = $(VERSION)-$(SHORT_DATE)
DEB_PATH = debian/$(APP_NAME)
DEB_PKG = debian/$(APP_NAME).deb
DEB_PKG_FINAL = debian/$(APP_NAME)_$(DEB_VERSION)_all.deb


ui: omniview/ui_about_dialog.py \
	omniview/ui_main_window.py \
	omniview/ui_info_dialog.py

omniview/ui_about_dialog.py: omniview/ui_about_dialog.ui
	m4  -D M4_APP_VERSION="$(VERSION)" \
		-D M4_GIT_REVISION="$(CI_COMMIT_SHORT_SHA)" \
		-D M4_COMPILE_DATE="$(COMPILE_DATE)" \
		-P omniview/ui_about_dialog.ui >omniview/ui_about_dialog.tmp.ui
	pyuic5 -o omniview/ui_about_dialog.py omniview/ui_about_dialog.tmp.ui

omniview/ui_info_dialog.py: omniview/ui_info_dialog.ui
	pyuic5 -o omniview/ui_info_dialog.py omniview/ui_info_dialog.ui

omniview/ui_main_window.py: omniview/ui_main_window.ui
	pyuic5 -o omniview/ui_main_window.py omniview/ui_main_window.ui

test: test-mypy test-unit

test-mypy: ui
	mypy --disable-error-code "annotation-unchecked" --ignore-missing-imports omniview/*.py omniview/plugins/*.py

test-unit: ui
	python3 test.py

pip-install:
	sed <setup.py.in "s/{{VERSION}}/$(VERSION)/" >setup.py
	pip3 install .

pip-uninstall:
	pip3 uninstall omniview

deb: clean deb-clean ui
	install -d $(DEB_PATH)/usr/bin
	install -m 755 bin/omniview $(DEB_PATH)/usr/bin/omniview

	install -d $(DEB_PATH)/usr/lib/python3/dist-packages/omniview
	cp -r omniview/*.py $(DEB_PATH)/usr/lib/python3/dist-packages/omniview/

	install -d $(DEB_PATH)/usr/lib/python3/dist-packages/omniview/plugins
	cp -r omniview/plugins/*.py $(DEB_PATH)/usr/lib/python3/dist-packages/omniview/plugins/

# 	mkdir -p $(DEB_PATH)/usr/share/icons/hicolor/16x16/apps
# 	cp images/icon-16.png $(DEB_PATH)/usr/share/icons/hicolor/16x16/apps/omniview.png

	mkdir -p $(DEB_PATH)/usr/share/applications
	cp se.entropic.omniview.desktop $(DEB_PATH)/usr/share/applications/

	mkdir -p $(DEB_PATH)/usr/share/doc/$(APP_NAME)
	cp LICENSE $(DEB_PATH)/usr/share/doc/$(APP_NAME)/copyright
	cp README.md $(DEB_PATH)/usr/share/doc/$(APP_NAME)/

	install -d $(DEB_PATH)/DEBIAN
	sed <debian/control.tpl "s/{VERSION}/$(DEB_VERSION)/" >$(DEB_PATH)/DEBIAN/control
	dpkg-deb --root-owner-group --build $(DEB_PATH)
	mv $(DEB_PKG) $(DEB_PKG_FINAL)

deb-repo: deb
	update-deb-repo.sh $(DEB_PKG_FINAL)

clean:
	rm -f omniview/ui_info_dialog.py
	rm -f omniview/ui_about_dialog.py
	rm -f omniview/ui_main_window.py
	rm -f omniview/ui_about_dialog.tmp.ui
	rm -rf omniview/__pycache__
	rm -rf __pycache__

deb-clean:
	rm -rf $(DEB_PATH)
	rm -rf debian/*.deb
