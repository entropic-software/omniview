#!/usr/bin/env python3

import io
import re
import unittest
from PIL import ImageQt

from omniview.common import *
from omniview.plugins.archive import Plugin as Archive
from omniview.plugins.text_markdown import Plugin as TextMarkdown
from omniview.plugins.text_html import Plugin as TextHtml
from omniview.plugins.text import Plugin as Text
from omniview.plugins.image import Plugin as Image
from omniview.plugins.video import Plugin as Video
from omniview.plugins.audio import Plugin as Audio
from omniview.plugins.application_octetstream import Plugin as ApplicationOctetstream
from omniview.plugins.application_x_iso9660_image import Plugin as ApplicationXIso9660Image

TESTFILE_PNG = "testfiles/pixel.png"
TESTFILE_XPM = "testfiles/pixel.xpm"

TESTFILE_RANDOM = "testfiles/random.bin"

TESTFILE_TXT = "testfiles/text-unicode.txt"
TESTFILE_TXT_ISO8859_1 = "testfiles/text-iso8859-1.txt"
TESTFILE_HTML = "testfiles/text-unicode.html"
TESTFILE_HTML_ISO8859_1 = "testfiles/text-iso8859-1.html"
TESTFILE_MARKDOWN = "testfiles/text-unicode.md"
TESTFILE_MARKDOWN_ISO8859_1 = "testfiles/text-iso8859-1.md"

TESTFILE_PDF_1P = "testfiles/pdf_1p.pdf"
TESTFILE_PDF_2P = "testfiles/pdf_2p.pdf"

TESTFILE_AUDIO = "testfiles/audio.wav"

TESTFILE_ZIP = "testfiles/archive.zip"
TESTFILE_RAR = "testfiles/archive.rar"
TESTFILE_TGZ = "testfiles/archive.tgz"
TESTFILE_TAR = "testfiles/archive.tar"

TESTFILE_CD_IMAGE = "testfiles/cdimage.iso"

class TestFunctions(unittest.TestCase):

    def _progress_dummy(self, value: int):
        pass


    ### File type

    # compressed

    def test_get_mimetype_gz(self):
        got = get_mimetype(TESTFILE_HTML+'.gz')
        expected = {
            'mime_type':    'text/html',
            'compression':  'gzip',
        }
        self.assertEqual(expected, got)

    def test_get_mimetype_bz2(self):
        got = get_mimetype(TESTFILE_HTML+'.bz2')
        expected = {
            'mime_type':    'text/html',
            'compression':  'bzip2',
        }
        self.assertEqual(expected, got)

    # normal

    def test_get_mimetype(self):
        got = get_mimetype(TESTFILE_HTML)
        expected = {
            'mime_type':    'text/html',
            'compression':  None,
        }
        self.assertEqual(expected, got)

    def test_get_mimetype_md(self):
        got = get_mimetype(TESTFILE_MARKDOWN)
        expected = {
            'mime_type':    'text/markdown',
            'compression':  None,
        }
        self.assertEqual(expected, got)

    def test_get_mimetype_txt(self):
        got = get_mimetype(TESTFILE_TXT)
        expected = {
            'mime_type':    'text/plain',
            'compression':  None,
        }
        self.assertEqual(expected, got)

    def test_get_mimetype_png(self):
        got = get_mimetype(TESTFILE_PNG)
        expected = {
            'mime_type':    'image/png',
            'compression':  None,
        }
        self.assertEqual(expected, got)

    def test_get_mimetype_xpm(self):
        got = get_mimetype(TESTFILE_XPM)
        expected = {
            'mime_type':    'image/x-xpixmap',
            'compression':  None,
        }
        self.assertEqual(expected, got)

    def test_get_mimetype_binary(self):
        got = get_mimetype(TESTFILE_RANDOM)
        expected = {
            'mime_type':    'application/octet-stream',
            'compression':  None,
        }
        self.assertEqual(expected, got)

    def test_get_mimetype_audio(self):
        got = get_mimetype(TESTFILE_AUDIO)
        expected = {
            'mime_type':    'audio/x-wav',
            'compression':  None,
        }
        self.assertEqual(expected, got)

    def test_get_mimetype_zip(self):
        got = get_mimetype(TESTFILE_ZIP)
        expected = {
            'mime_type':    'application/zip',
            'compression':  None,
        }
        self.assertEqual(expected, got)

    def test_get_mimetype_tgz(self):
        got = get_mimetype(TESTFILE_TGZ)
        expected = {
            'mime_type':    'application/x-tar',
            'compression':  'gzip',
        }
        self.assertEqual(expected, got)

    def test_get_mimetype_tar(self):
        got = get_mimetype(TESTFILE_TAR)
        expected = {
            'mime_type':    'application/x-tar',
            'compression':  None,
        }
        self.assertEqual(expected, got)

    def test_get_mimetype_iso(self):
        got = get_mimetype(TESTFILE_CD_IMAGE)
        expected = {
            'mime_type':    'application/x-iso9660-image',
            'compression':  None,
        }
        self.assertEqual(expected, got)


    ### Plugin loading and basic plugin functions

    def test_find_plugin_unimplemented_filetype(self):
        with self.assertRaises(RuntimeWarning):
            p = find_plugin('none/none')

    def test_find_plugin_text(self):
        p = find_plugin('text/plain')
        self.assertEqual(2, p.prio())
        self.assertEqual(True, p.needs_data())
        self.assertEqual(True, p.matches_mime_type('text/whatever'))
        self.assertEqual(DISPLAY_PLAIN_TEXT, p.get_display())
        with self.assertRaises(NotImplementedError):
            num_pages = p.get_num_pages()

    def test_find_plugin_pdf_1(self):
        with open(TESTFILE_PDF_1P, "rb") as file:
            data = file.read()
        p = find_plugin('application/pdf')
        p.set_data(data, TESTFILE_PDF_1P)
        self.assertEqual(1, p.prio())
        self.assertEqual(False, p.needs_data())
        self.assertEqual(True, p.matches_mime_type('application/pdf'))
        self.assertEqual(DISPLAY_DOCUMENT, p.get_display())
        self.assertEqual(1, p.get_num_pages())

    def test_find_plugin_pdf_2(self):
        with open(TESTFILE_PDF_2P, "rb") as file:
            data = file.read()
        p = find_plugin('application/pdf')
        p.set_data(data, TESTFILE_PDF_2P)
        self.assertEqual(1, p.prio())
        self.assertEqual(False, p.needs_data())
        self.assertEqual(True, p.matches_mime_type('application/pdf'))
        self.assertEqual(DISPLAY_DOCUMENT, p.get_display())
        self.assertEqual(2, p.get_num_pages())



    ### Plugin data processing functions

    # Binary

    def test_plugin_binary_type(self):
        data = b"hello"
        expected = DISPLAY_PLAIN_TEXT
        plugin = ApplicationOctetstream()
        plugin.set_data(data, 'filename')
        got = plugin.get_display()
        self.assertEqual(expected, got)

    def test_plugin_binary_data(self):
        data = b"hello hello hello hello hello hello\x01\xff"
        expected  = "00000000 68 65 6C 6C 6F 20 68 65 6C 6C 6F 20 68 65 6C 6C hello hello hell\n"
        expected += "00000010 6F 20 68 65 6C 6C 6F 20 68 65 6C 6C 6F 20 68 65 o hello hello he\n"
        expected += "00000020 6C 6C 6F 01 FF llo..\n"
        plugin = ApplicationOctetstream()
        plugin.set_data(data, 'filename')
        got = plugin.get_processed_data(self._progress_dummy).decode("utf-8")
        # compress spaces to simplify comparison and just  compare the most
        # important data
        got = re.sub("[ ]+", " ", got)
        self.assertEqual(expected, got)

    # Archive
    
    def test_plugin_archive_type(self):
        with open(TESTFILE_ZIP, "rb") as file:
            data = file.read()
        expected = DISPLAY_PLAIN_TEXT
        plugin = Text()
        plugin.set_data(data, TESTFILE_ZIP)
        got = plugin.get_display()
        self.assertEqual(expected, got)

    # CD-image

    def test_plugin_valid_cdimage(self):
        data = None
        expected = b'/\n/FILE.TXT;1\n'
        plugin = ApplicationXIso9660Image()
        plugin.set_data(data, TESTFILE_CD_IMAGE)
        got = plugin.get_processed_data(self._progress_dummy)
        self.assertEqual(expected, got)

    def test_plugin_invalid_cdimage(self):
        data = None
        plugin = ApplicationXIso9660Image()
        plugin.set_data(data, TESTFILE_RANDOM)
        with self.assertRaises(RuntimeWarning):
            plugin.get_processed_data(self._progress_dummy)

    # Audio / Video

    def test_plugin_audio_type(self):
        data = None
        expected = DISPLAY_AUDIO
        plugin = Audio()
        plugin.set_data(data, 'filename')
        got = plugin.get_display()
        self.assertEqual(expected, got)

    def test_plugin_audio_data(self):
        data = None
        expected = b''
        plugin = Audio()
        plugin.set_data(data, 'filename')
        got = plugin.get_processed_data(self._progress_dummy)
        self.assertEqual(expected, got)

    def test_plugin_video_type(self):
        data = None
        expected = DISPLAY_VIDEO
        plugin = Video()
        plugin.set_data(data, 'filename')
        got = plugin.get_display()
        self.assertEqual(expected, got)

    def test_plugin_video_data(self):
        data = None
        expected = b''
        plugin = Video()
        plugin.set_data(data, 'filename')
        got = plugin.get_processed_data(self._progress_dummy)
        self.assertEqual(expected, got)

    # Image

    def test_plugin_image_type(self):
        with open(TESTFILE_XPM, "rb") as file:
            data = file.read()
        expected = DISPLAY_IMAGE
        plugin = Image()
        plugin.set_data(data, TESTFILE_XPM)
        got = plugin.get_display()
        self.assertEqual(expected, got)

    def test_plugin_image_data_xpm(self):
        with open(TESTFILE_XPM, "rb") as file:
            data = file.read()
        plugin = Image()
        plugin.set_data(data, TESTFILE_XPM)
        got = plugin.get_processed_data(self._progress_dummy)
        self.assertEqual(got, data)

    def test_plugin_image_data_png(self):
        with open(TESTFILE_PNG, "rb") as file:
            data = file.read()
        plugin = Image()
        plugin.set_data(data, TESTFILE_PNG)
        got = plugin.get_processed_data(self._progress_dummy)
        self.assertEqual(got, data)


    # Text (plain)

    def test_plugin_text_type(self):
        with open(TESTFILE_TXT, "rb") as file:
            data = file.read()
        expected = DISPLAY_PLAIN_TEXT
        plugin = Text()
        plugin.set_data(data, TESTFILE_TXT)
        got = plugin.get_display()
        self.assertEqual(expected, got)

    def test_plugin_text_data(self):
        with open(TESTFILE_TXT, "rb") as file:
            data = file.read()
        expected = data.decode("utf-8")
        plugin = Text()
        plugin.set_data(data, TESTFILE_TXT)
        got = plugin.get_processed_data(self._progress_dummy).decode("utf-8")
        self.assertEqual(expected, got)

    def test_plugin_text_autodetect_iso8859_1(self):
        with open(TESTFILE_TXT_ISO8859_1, "rb") as file:
            data = file.read()
        expected = codecs.decode(data, encoding="iso8859-1")
        plugin = Text()
        plugin.set_data(data, TESTFILE_TXT_ISO8859_1)
        got = plugin.get_processed_data(self._progress_dummy).decode("utf-8")
        self.assertEqual(expected, got)


    # Text (html)

    def test_plugin_html_type(self):
        with open(TESTFILE_HTML, "rb") as file:
            data = file.read()
        expected = DISPLAY_RICH_TEXT
        plugin = TextHtml()
        plugin.set_data(data, TESTFILE_HTML)
        got = plugin.get_display()
        self.assertEqual(expected, got)

    def test_plugin_html_data(self):
        with open(TESTFILE_HTML, "rb") as file:
            data = file.read()
        expected = data.decode("utf-8")
        plugin = TextHtml()
        plugin.set_data(data, TESTFILE_HTML)
        got = plugin.get_processed_data(self._progress_dummy).decode("utf-8")
        self.assertEqual(expected, got)

    def test_plugin_html_autodetect_iso8859_1(self):
        with open(TESTFILE_HTML_ISO8859_1, "rb") as file:
            data = file.read()
        expected = codecs.decode(data, encoding="iso8859-1")
        plugin = TextHtml()
        plugin.set_data(data, TESTFILE_HTML_ISO8859_1)
        got = plugin.get_processed_data(self._progress_dummy).decode("utf-8")
        self.assertEqual(expected, got)

    # Text (markdown)

    def test_plugin_markdown_type(self):
        with open(TESTFILE_MARKDOWN, "rb") as file:
            data = file.read()
        expected = DISPLAY_RICH_TEXT
        plugin = TextMarkdown()
        plugin.set_data(data, TESTFILE_MARKDOWN)
        got = plugin.get_display()
        self.assertEqual(expected, got)

    def test_plugin_markdown_data(self):
        with open(TESTFILE_MARKDOWN, "rb") as file:
            data = file.read()
        expected  = "<h1>This is a unicode md file</h1>\n"
        expected += "<p>Göteborg Växjö Åland España Київ Україна 東京 日本 北京 中国 Αθήνα Ελληνική ημοκρατία</p>\n"
        expected += "<p><strong>bold</strong> <em>italic</em></p>"
        plugin = TextMarkdown()
        plugin.set_data(data, TESTFILE_MARKDOWN)
        got = plugin.get_processed_data(self._progress_dummy).decode("utf-8")
        self.assertEqual(expected, got)

    def test_plugin_markdown_autodetect_iso8859_1(self):
        with open(TESTFILE_MARKDOWN_ISO8859_1, "rb") as file:
            data = file.read()
        expected  = "<h1>This is a iso8859-1 md file</h1>\n"
        expected += "<p>Göteborg, Växjö, Åland, España</p>\n"
        expected += "<p><strong>bold</strong> <em>italic</em></p>"
        plugin = TextMarkdown()
        plugin.set_data(data, TESTFILE_MARKDOWN_ISO8859_1)
        got = plugin.get_processed_data(self._progress_dummy).decode("utf-8")
        self.assertEqual(expected, got)


    # Format size

    def test_format_size(self):
        comparisons = {
            1:              "1 B",
            512:            "512 B",
            1023:           "1023 B",
            1024:           "1 KiB",
            (1024**2-1):    "1024 KiB",
            (1024**2):      "1 MiB",
            (1024**3-1):    "1024 MiB",
            (1024**3):      "1 GiB",
            (1024**4-1):    "1024 GiB",
            (1024**4):      "1 TiB",
            (1024**5-1):    "1024 TiB",
            (1024**5):      "1 PiB",
            (1024**6-1):    "1024 PiB",
            (1024**6):      "1024 PiB",
            (1024**7-1):    "1048576 PiB",
            (1024**7):      "1048576 PiB",
            (1024**8-1):    "1073741824 PiB",
            (1024**8):      "1073741824 PiB",
        }
        for bytes_, label in comparisons.items():
            expected = label
            got = format_size(bytes_)
            self.assertEqual(expected, got)


    # Build command

    def test_build_command_with_variable_at_end(self):
        cmd_tpl = "firefox --new-window %u"
        arg = "https://python.org/"

        expected = [
            "firefox",
            "--new-window",
            "https://python.org/",
        ]
        got = build_command(cmd_tpl, arg)
        self.assertEqual(expected, got)


    def test_build_command_with_variable_inside(self):
        cmd_tpl = "firefox %u --new-window"
        arg = "https://python.org/"

        expected = [
            "firefox",
            "https://python.org/",
            "--new-window",
        ]
        got = build_command(cmd_tpl, arg)
        self.assertEqual(expected, got)


    def test_build_command_without_variable(self):
        cmd_tpl = "firefox"
        arg = "https://python.org/"

        expected = [
            "firefox",
            "https://python.org/",
        ]
        got = build_command(cmd_tpl, arg)
        self.assertEqual(expected, got)


    def test_build_command_with_multiple_variables_at_end(self):
        # Replaces only the first tpl var
        cmd_tpl = "firefox --new-window %u %u"
        arg = "https://python.org/"

        expected = [
            "firefox",
            "--new-window",
            "https://python.org/",
            "%u",
        ]
        got = build_command(cmd_tpl, arg)
        self.assertEqual(expected, got)


    def test_build_command_with_multiple_variables_inside(self):
        # Replaces only the first tpl var
        cmd_tpl = "firefox %u --new-window %u"
        arg = "https://python.org/"

        expected = [
            "firefox",
            "https://python.org/",
            "--new-window",
            "%u",
        ]
        got = build_command(cmd_tpl, arg)
        self.assertEqual(expected, got)


if __name__ == '__main__':
    unittest.main()
