# Plugin interface

Example:

```python
# Important constants and functions
from omniview.common import *
from omniview.plugin_base import PluginBase
from typing import Dict

class Plugin(PluginBase):
    # 1: highest prio, 2 lower, etc
    # A plugin with a higher prio overrides plugins which matches the same
    # mime-type(s) but has a lower priority
    def prio(self) -> int:
        return 1

    # Whether the plugin works on file data or the file name
    def needs_data(self) -> bool:
        return True

    # Return true if the argument matches a specific mime-type
    def matches_mime_type(self, t: str) -> bool:
        return t == 'application/text'
        # or
        return t.begins_with('application/')
        # or whatever matching logic is needed

    def set_data(self, data: bytes, filename: str):
        self.data: bytes = data
        # or
        self.filename: str = filename
        # or both
        # other init code may be placed here too

    # Return one of the constants defined in common.py
    # This decides which display and controls are used to display the data
    def get_display(self) -> str:
        return DISPLAY_PLAIN_TEXT

    # Process the data and return it for display in one of the displays.
    # See below for display types.
    # 'option' arg is used for plugin type specific options, for example
    # page number in the case of documents.
    def get_processed_data(self, options: Dict = {}) -> str:
        return self.data.decode("utf-8")
```


## Displays

TODO
