# Omniview

Amiga Multiview inspired document viewer for Python 3

## Screenshot

![Screenshot](screenshot.png)

## Configuration

There is no preferences GUI yet, but there is a configuration file that can
be edited manually. It is situated at `~/.config/Entropic/omniview.conf`

### Browser

One item that you may need to configure is what web-browser to use when
clicking links in an html-file. If not configured, Omniview will use
`sensible-browser` which might or might not work for you.
Add a "WebBrowser" value to the "General" section like this:
```
[General]
WebBrowser=firefox --new-window %u
```

### Keyboard shortcuts

You might also want to change the default keyboard shortcuts. In that case
it's easiest to start and then quit Omniview once, so the default shortcuts 
will be written to the configuration file. Then you can change the shortcuts
in the configuration file.


## Installation

Easiest way to install on Debian is from my repo: https://entropic.se/debian

You can also install with pip, but then you'll need to install poppler-utils
yourself. Run `make pip-install` to install as user, or `sudo make pip-install`
to install system-wide.

## Requirements

- Python 3.7
- Qt 5.11
- PyQt5

(see debian/control.tpl for detailed dependency list)

Recommended:
- python3-markdown for markdown support
- python3-pil for image support
- python3-pyqt5.qtmultimedia for video and audio support
- poppler-utils for pdf support

Optional:
- python3-ezdxf for dxf support (not usable yet)

### Requirements for building

`apt install pyqt5-dev-tools m4 make`

## Author

Created by Tomas Åkesson <tomas@entropic.se>

See LICENSE for license details.

Project homepage: https://gitlab.com/entropic-software/omniview
