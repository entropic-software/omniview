Package: omniview
Version: {VERSION}
Section: text
Priority: optional
Architecture: all
Depends: python3, python3-pyqt5, python3-markdown, python3-magic, python3-pil, python3-pyqt5.qtmultimedia, libqt5multimedia5-plugins, poppler-utils, python3-chardet, python3-psutil
Suggests: python3-ezdxf, atool, python3-pycdlib
Maintainer: Tomas Åkesson <tomas@entropic.se>
Homepage: https://gitlab.com/entropic-software/omniview
Description: Amiga Multiview inspired document viewer
 Can load and display many types of documents: text, images, video, audio.
 Support for plugins to add more filetypes.
 .
 Can load the following file types:
 plain text, markdown text, html, images supported by PIL, pdf, audio and
 video supported by qtmultimedia
 .
 Suggested packages:
 - python3-ezdxf for displaying dxf files (cad)
 - atool for displaying contents of a number of archives
