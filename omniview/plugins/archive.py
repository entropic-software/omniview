from omniview.common import *
from omniview.plugin_base import PluginBase
from typing import Dict, List, Callable

class Plugin(PluginBase):
    def prio(self) -> int:
        return 2

    def needs_data(self) -> bool:
        return False

    def matches_mime_type(self, t: str) -> bool:
        return (
            t == 'application/x-gtar-compressed' or
            t == 'application/x-tar' or
            t == 'application/x-xz' or
            t == 'application/vnd.rar' or
            t == 'application/zip' or
            t == 'application/x-lha'
        )

    def set_data(self, data: bytes, filename: str):
        self.filename: str = filename

    def get_display(self) -> str:
        return DISPLAY_PLAIN_TEXT

    def get_processed_data(
        self,
        options: Dict = {}
    ) -> bytes:
        import subprocess

        listing: bytearray = bytearray()

        command: List = ["atool", "-l", self.filename]
        try:
            out: bytes = subprocess.check_output(command)
            for row in out:
                if self.interrupted:
                    raise InterruptedError()
                listing.append(row)

        except subprocess.CalledProcessError as e:
            raise RuntimeWarning("Failed to run command: "+str(e))

        return bytes(listing)
