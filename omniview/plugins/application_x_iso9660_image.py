from omniview.common import *
from omniview.plugin_base import PluginBase
from typing import Dict, List, Callable

class Plugin(PluginBase):
    def prio(self) -> int:
        return 2

    def needs_data(self) -> bool:
        return False

    def matches_mime_type(self, t: str) -> bool:
        return (
            t == 'application/x-iso9660-image'
        )

    def set_data(self, data: bytes, filename: str):
        self.filename: str = filename

    def get_display(self) -> str:
        return DISPLAY_PLAIN_TEXT

    def get_processed_data(
        self,
        options: Dict = {}
    ) -> bytes:
        try:
            import pycdlib
        except ModuleNotFoundError as e:
            # TODO: Create a separate exception for this purpose
            raise RuntimeWarning("External package needed: pycdlib")

        # TODO: OPNsense-21.1-OpenSSL-dvd-amd64.iso

        self.signals.progress.emit(0) # type: ignore
        listing: str = ""
        iso = pycdlib.PyCdlib()

        try:
            iso.open(self.filename)
            for root, dirs, files in iso.walk(iso_path='/'):
                listing += f"{root}\n"
                for file in files:
                    if self.interrupted: raise InterruptedError()
                    if root == "/":
                        listing += f"/{file}\n"
                    else:
                        listing += f"{root}/{file}\n"
        except pycdlib.pycdlibexception.PyCdlibInvalidInput as e:
            if "OMNIVIEW_DEBUG" in os.environ: print(f"pycdlib error: {e}")
            raise RuntimeWarning(f"Failed to load iso-file")
        except pycdlib.pycdlibexception.PyCdlibInvalidISO as e:
            if "OMNIVIEW_DEBUG" in os.environ: print(f"pycdlib error: {e}")
            raise RuntimeWarning(f"Failed to load iso-file")
        except Exception as e:
            raise(e)

        self.signals.progress.emit(100) # type: ignore
        return listing.encode("utf-8")
