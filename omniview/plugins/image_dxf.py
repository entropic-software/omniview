# info
# https://ezdxf.readthedocs.io/en/stable/
# https://github.com/mozman/ezdxf/discussions/550

from omniview.common import *
from omniview.plugin_base import PluginBase
from typing import Dict, Callable

class Plugin(PluginBase):
    def prio(self) -> int:
        return 1

    def needs_data(self) -> bool:
        return True

    def matches_mime_type(self, t: str) -> bool:
        return t == 'image/vnd.dxf'

    def set_data(self, data: bytes, filename: str):
        self.data: bytes = data

    def get_display(self) -> str:
        return DISPLAY_IMAGE

    def get_processed_data(
        self,
        options: Dict = {}
    ) -> bytes:
        try:
            import ezdxf
            import ezdxf.recover
            from ezdxf import read, DXFStructureError
        except ModuleNotFoundError as e:
            # TODO: Create a separate exception for this purpose
            raise RuntimeWarning("External package needed: ezdxf")

        import io

        try:
            data = ezdxf.read(io.StringIO(self.data.decode("utf-8")))
            # data = ezdxf.recover.read(io.BytesIO(self.data))
            
            raise RuntimeWarning("TODO: implement dxf filetype")

        except DXFStructureError as e:
            raise RuntimeWarning(f"Failed to load dxf file: {e}")
