from omniview.common import *
from omniview.plugin_base import PluginBase
from typing import List, Dict, Callable

class Plugin(PluginBase):
    def prio(self) -> int:
        return 1

    def needs_data(self) -> bool:
        return False

    def matches_mime_type(self, t: str) -> bool:
        return t == 'application/vnd.debian.binary-package'

    def set_data(self, data: bytes, filename: str):
        self.filename: str = filename

    def get_display(self) -> str:
        return DISPLAY_PLAIN_TEXT

    def get_processed_data(
        self,
        options: Dict = {}
    ) -> bytes:
        import subprocess

        listing: bytearray = bytearray()

        try:
            command: List = ["dpkg-deb", "-I", self.filename]
            out: bytes = subprocess.check_output(command)
            for row in out:
                if self.interrupted: return b''
                listing.append(row)

            listing.append(13) # newline

            command = ["dpkg-deb", "-c", self.filename]
            out = subprocess.check_output(command)
            for row in out:
                if self.interrupted: return b''
                listing.append(row)

        except subprocess.CalledProcessError as e:
            raise RuntimeWarning("Failed to run command: "+str(e))

        return bytes(listing)
