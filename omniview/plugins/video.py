from omniview.common import *
from omniview.plugin_base import PluginBase
from typing import Dict, Callable

class Plugin(PluginBase):
    def prio(self) -> int:
        return 2

    def needs_data(self) -> bool:
        return False

    def matches_mime_type(self, t: str) -> bool:
        return t.startswith('video/')

    def set_data(self, data: bytes, filename: str):
        pass

    def get_display(self) -> str:
        return DISPLAY_VIDEO

    def get_processed_data(
        self,
        options: Dict = {}
    ) -> bytes:
        return b''
