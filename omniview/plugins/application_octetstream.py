from omniview.common import *
from omniview.plugin_base import PluginBase
from typing import Dict, Callable

class Plugin(PluginBase):
    def prio(self) -> int:
        return 2

    def needs_data(self) -> bool:
        return True

    def matches_mime_type(self, t: str) -> bool:
        return t.startswith('application/') or t == "inode/x-empty"

    def set_data(self, data: bytes, filename: str):
        self.data: bytes = data

    def get_display(self) -> str:
        return DISPLAY_PLAIN_TEXT

    def get_processed_data(
        self,
        options: Dict = {}
    ) -> bytes:
        data: bytes = self.data
        text: str = ""
        bytes_per_row: int = 16
        bytes_read: int = 0

        file_size: int = len(data)
        self.signals.progress.emit(0) # type: ignore

        for chunk in [data[i : i + 16] for i in range(0, len(data), 16)]:
            if self.interrupted:
                raise InterruptedError()

            hchars: str = ""
            chars: str = ""
            i: int = 0
            for byte in chunk:
                i += 1

                if byte >= 32 and byte <= 126:
                    char = chr(byte)
                else:
                    char = '.'
                chars += char

                hchars += "%0.2X" % byte
                if i % (bytes_per_row / 4) == 0:
                    hchars += "  "
                else:
                    hchars += " "

            hfill: int = bytes_per_row - len(chunk)
            for i in range(hfill):
                hchars += "  "
                if i % (bytes_per_row / 4) == 0:
                    hchars += "  "
                else:
                    hchars += " "

            addr: str = "%0.8X" % bytes_read
            text += f"{addr}   {hchars}{chars}\n"
            bytes_read += bytes_per_row

            self.signals.progress.emit(int(bytes_read / file_size * 100)) # type: ignore

            # TODO: temporary hard limit just to avoid freezing
            if bytes_read > 50000000:
                raise MemoryError()

        self.signals.progress.emit(100) # type: ignore
        return text.encode("utf-8")
