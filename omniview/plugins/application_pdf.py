"""
Inspired by pdf2images by Edouard Belval https://github.com/Belval/pdf2image

MIT License

Copyright (c) 2022 Tomas Åkesson
Copyright (c) 2017 Edouard Belval

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import io
import psutil
import subprocess
import time
from omniview.common import *
from omniview.plugin_base import PluginBase
from PyQt5 import QtGui, QtCore
from typing import Dict, List, Optional, Callable

class Plugin(PluginBase):
    def prio(self) -> int:
        return 1

    def needs_data(self) -> bool:
        return False

    def supports_compression(self) -> bool:
        return False

    def matches_mime_type(self, t: str) -> bool:
        return t == 'application/pdf'

    def set_data(self, data: bytes, filename: str):
        self.filename: str = filename
        self.data: bytes = data
        self.num_pages: Optional[int] = None
        self.page_size: Optional[int] = None
        self.page_cache: Dict = {}

    def get_display(self) -> str:
        return DISPLAY_DOCUMENT

    # param: optional options['page_num'], 1 if unset
    def get_processed_data(
        self,
        options: Dict = {}
    ) -> bytes:
        from PIL import Image as PILImage, ImageQt
        page_num: int = 1
        if 'page_num' in options:
            page_num = options['page_num']

        try:
            num_pages: int = self.get_num_pages()
            if page_num < 1 or page_num > num_pages:
                raise IndexError(f"Page number out of range {page_num} / {num_pages}")

            return self._load_page_cached(page_num)

        except (subprocess.CalledProcessError, IOError) as e:
            print(str(e))
            raise RuntimeWarning(f"Failed to read pdf file")


    def get_num_pages(self) -> int:
        if self.num_pages is not None:
            return self.num_pages

        command: List = ["pdfinfo", self.filename]
        out: bytes = subprocess.check_output(command)
        for row in out.decode("utf8", "ignore").split("\n"):
            sf: List = row.split(":")
            key, value = sf[0], ":".join(sf[1:])
            if key == 'Pages':
                self.num_pages = int(value.strip())
                return self.num_pages

        if not self.num_pages:
            raise ValueError

    def _load_page_cached(self, page_num: int) -> bytes:
        # Expunge items from cache
        cache_size: int = 0
        for x in self.page_cache:
            cache_size += self.page_cache[x]['size']

        if cache_size > self._page_cache_mem_limit():
            oldest_access: Optional[float] = None
            oldest_key: Optional[str] = None
            for x in self.page_cache:
                lastaccess: float = self.page_cache[x]['lastaccess']
                if not oldest_access or lastaccess < oldest_access:
                    oldest_access = lastaccess
                    oldest_key = x

            if oldest_key:
                del self.page_cache[oldest_key]

        # Load current page and save it in the cache
        if not page_num in self.page_cache:
            image_data: bytes = self._load_page(page_num)
            self.page_cache[page_num] = {
                'data': image_data,
                'size': len(image_data),
                'lastaccess': time.monotonic(),
            }

        self.page_cache[page_num]['lastaccess'] = time.monotonic()

        return self.page_cache[page_num]['data']


    # Mem limit based on half the available memory
    def _page_cache_mem_limit(self) -> int:
        return int(psutil.virtual_memory().available / 2)


    def _load_page(self, page_num) -> bytes:
        command: List = [
            "pdftocairo",
            "-scale-to", str(self._calc_page_size()),
            "-f", str(page_num),
            "-l", str(page_num),
            "-singlefile",
            "-jpeg", "-jpegopt", "quality=100",
            self.filename, "-",
        ]

        process = subprocess.Popen(
            command,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        self.pid = process.pid
        stdout, _ = process.communicate()
        output: bytearray = bytearray()
        for byte in stdout:
            if self.interrupted:
                return b''
            output.append(byte)

        return bytes(output)


    def _calc_page_size(self) -> int:
        if self.page_size is not None:
            return self.page_size

        rect: QtCore.QRect = QtGui.QGuiApplication.primaryScreen().geometry()
        scr_h: int = rect.height()
        scr_w: int = rect.width()
        self.page_size = max(scr_h, scr_w)
        return self.page_size
