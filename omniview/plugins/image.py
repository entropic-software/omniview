from omniview.common import *
from omniview.plugin_base import PluginBase
from typing import Dict, Callable

class Plugin(PluginBase):
    def prio(self) -> int:
        return 2

    def needs_data(self) -> bool:
        return True

    def matches_mime_type(self, t: str) -> bool:
        return t.startswith('image/')

    def set_data(self, data: bytes, filename: str):
        self.data: bytes = data

    def get_display(self) -> str:
        return DISPLAY_IMAGE

    def get_processed_data(
        self,
        options: Dict = {}
    ) -> bytes:
        return self.data
