from omniview.common import *
from omniview.plugin_base import PluginBase
from typing import Dict, Callable

class Plugin(PluginBase):
    def prio(self) -> int:
        return 2

    def needs_data(self) -> bool:
        return True

    def matches_mime_type(self, t: str) -> bool:
        return (
            t.startswith('text/') or
            t == 'message/rfc822' or
            t == 'application/pgp-keys' or
            t == 'application/json' or
            t == 'application/x-ruby'
        )

    def set_data(self, data: bytes, filename: str):
        self.data: bytes = data

    def get_display(self) -> str:
        return DISPLAY_PLAIN_TEXT

    def get_text_encoding(self) -> str:
        return self.text_encoding

    def get_processed_data(
        self,
        options: Dict = {}
    ) -> bytes:
        decoded: Tuple[str, str] = decode_text(self.data)
        decoded_data: str = decoded[0]
        self.text_encoding: str = decoded[1]
        return decoded_data.encode("utf-8")
