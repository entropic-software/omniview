from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

from omniview.ui_about_dialog import Ui_AboutDialog


class AboutDialog(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = Ui_AboutDialog()
        self.ui.setupUi(self)
        self.ui.but_about_close.clicked.connect(self.accept)
