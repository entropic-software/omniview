from datetime import datetime
import bz2
import gzip
import io
import os
from PyQt5.QtCore import QObject, QRunnable, pyqtSignal, pyqtSlot
from PyQt5.QtWidgets import QApplication
from typing import Optional
from omniview.plugin_base import PluginBase


class LoadFileWorkerSignals(QObject):
    finished = pyqtSignal()
    result = pyqtSignal(bytes, str, str, str, str, PluginBase)
    error = pyqtSignal(str)
    status = pyqtSignal(str)
    progress = pyqtSignal(int)


class LoadFileWorker(QRunnable):
    def __init__(
        self,
        filename: str,
        mime_type: str,
        compression: Optional[str],
        plugin: PluginBase
    ):
        super(LoadFileWorker, self).__init__()
        self.filename: str = filename
        self.mime_type: str = mime_type
        self.compression: Optional[str] = compression
        self.plugin = plugin
        self.interrupted: bool = False
        self.signals: LoadFileWorkerSignals = LoadFileWorkerSignals()


    @pyqtSlot()
    def run(self):
        try:
            data: bytes = self._read_data()
            self.plugin.set_data(data, self.filename)

            self.signals.status.emit(QApplication.translate(
                "Omniview", "Processing file, please wait ..."
            ))

            processed_data = self.plugin.get_processed_data()

            if self.interrupted:
                message: str = QApplication.translate("Omniview", "Interrupted.")
                self.signals.error.emit(message)
            else:
                self.signals.result.emit(
                    processed_data,
                    self.filename,
                    self.mime_type,
                    self.compression,
                    self.plugin.get_text_encoding(),
                    self.plugin
                )

        except UnicodeDecodeError as e:
            message: str = QApplication.translate(
                "Omniview",
                "Error: Could not load file \"{0}\" as text"
            ).format(os.path.basename(self.filename))
            self.signals.error.emit(message)

        except MemoryError as e:
            message: str = QApplication.translate(
                "Omniview",
                "Error: Could not load file \"{0}\". Out of memory."
            ).format(os.path.basename(self.filename))
            self.signals.error.emit(message)

        except InterruptedError as e:
            message: str = QApplication.translate(
                "Omniview",
                "Error: Could not load file \"{0}\". Interrupted."
            ).format(os.path.basename(self.filename))
            self.signals.error.emit(message)

        except (RuntimeWarning, IsADirectoryError, PermissionError, ValueError) as e:
            message: str = QApplication.translate(
                "Omniview", f"Could not load file. {e}"
            )
            self.signals.error.emit(message)

        except Exception as e:
            if "OMNIVIEW_DEBUG" in os.environ:
                raise(e)
            else:
                self.signals.error.emit(str(e))

        finally:
            self.signals.finished.emit()


    def progress(self, value: int):
        self.signals.progress.emit(value) # type: ignore


    def stop(self):
        self.interrupted = True


    def _read_data(self) -> bytes:
        data: bytes = b''

        if self.plugin.needs_data():
            data = self.load_binary(self.filename)

        self.signals.progress.emit(100) # type: ignore
        return data


    def load_binary(self, filename: str) -> bytes:
        self.signals.progress.emit(0) # type: ignore

        if self.compression == 'bzip2':
            with bz2.open(filename, "rb") as bzfile:
                data = self.read_from_file(bzfile, filename)
                # data = bzfile.read()
        elif self.compression == 'gzip':
            with gzip.open(filename, "rb") as gzfile:
                data = self.read_from_file(gzfile, filename)
                # data = gzfile.read()
        else:
            with open(filename, "rb") as file:
                data = self.read_from_file(file, filename)
                # data = file.read()

        self.signals.progress.emit(100) # type: ignore

        return data


    def read_from_file(self, file, filename: str):
        data: bytearray = bytearray()

        file_size: int = os.path.getsize(filename)
        bytes_read: int = 0
        chunksize: int = 4096

        chunk: bytes = file.read(chunksize)
        bytes_read =+ chunksize
        while chunk:
            if self.interrupted: return b''

            progress_percent: int = int(bytes_read / file_size * 100)
            self.signals.progress.emit(progress_percent) # type: ignore
            data += chunk
            chunk = file.read(chunksize)
            bytes_read += chunksize

            # TODO: temporary hard limit just to avoid freezing
            if bytes_read > 500000000:
                raise MemoryError()

        return bytes(data)
