#!/usr/bin/env python3

import mimetypes
import sys
from PyQt5 import QtWidgets, QtGui
from PyQt5.QtWidgets import QApplication
from PyQt5.QtCore import QLocale, QTranslator, QCoreApplication, QTimer

import omniview
from omniview.main_window import MainWindow

def main() -> None:
    mimetypes.init()

    app = QtWidgets.QApplication(sys.argv)

    QCoreApplication.setOrganizationName("Entropic")
    QCoreApplication.setOrganizationDomain("entropic.se")
    QCoreApplication.setApplicationName("omniview")

    app.setWindowIcon(QtGui.QIcon.fromTheme("documentation"))

    locale = QLocale.system().name()
    tr = QTranslator()
    if tr.load("omniview" + locale, ":/i18n/"):
        QCoreApplication.installTranslator(tr)


    w = MainWindow()
    w.show()
    if len(sys.argv) > 1:
        w.load_file(sys.argv[1])

    # This lets the user press CTRL-C to kill the program even when it sleeps
    timer = QTimer()
    timer.timeout.connect(lambda: None) # type: ignore
    timer.start(100)

    sys.exit(app.exec_())

if __name__ == "__main__":
    main()
