from datetime import datetime
import io
import os
from pathlib import Path
import re
import subprocess
import sys
import time
from PIL import Image as PILImage, ImageQt
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import QFont, QTextCursor, QKeySequence
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtMultimedia import QMediaPlayer, QMediaContent
from PyQt5.QtMultimediaWidgets import QVideoWidget
from typing import Dict, List, Optional

from omniview.common import *
from omniview.load_file_worker import LoadFileWorker
from omniview.ui_main_window import Ui_MainWindow
from omniview.about_dialog import AboutDialog
from omniview.info_dialog import InfoDialog
from omniview.plugin_base import PluginBase


class MainWindow(QMainWindow, Ui_MainWindow):
    stop_load_file = pyqtSignal()

    def __init__(self, parent = None):
        QMainWindow.__init__(self, parent=parent)
        self.setupUi(self)

        self.threadpool: QThreadPool = QThreadPool()
        self.action_stop_threads = QtWidgets.QAction(self)

        # Add actions to window, otherwise they will be disabled when
        # toolbars and menus are hidden
        self.addAction(self.action_about)
        self.addAction(self.action_back_large)
        self.addAction(self.action_back_small)
        self.addAction(self.action_fullscreen)
        self.addAction(self.action_fwd_large)
        self.addAction(self.action_fwd_small)
        self.addAction(self.action_mute_audio)
        self.addAction(self.action_open)
        self.addAction(self.action_page_first)
        self.addAction(self.action_page_last)
        self.addAction(self.action_page_next)
        self.addAction(self.action_page_prev)
        self.addAction(self.action_file_first)
        self.addAction(self.action_file_last)
        self.addAction(self.action_file_next)
        self.addAction(self.action_file_prev)
        self.addAction(self.action_play_pause)
        self.addAction(self.action_quit)
        self.addAction(self.action_scale_to_window)
        self.addAction(self.action_toggle_font)
        self.addAction(self.action_view_as_binary)
        self.addAction(self.action_view_as_normal)
        self.addAction(self.action_view_plain_text)
        self.addAction(self.action_reload)

        # The other views are defined in the ui file, only QVideoWidget is
        # defined here, because Qt Designer has not support for it.
        self.video_view = QVideoWidget()
        self.video_view.setGeometry(self.pos().x(), self.pos().y(), self.width(), self.height())
        self.video_layout.addWidget(self.video_view)

        # For displaying page number
        self.progress_bar = QtWidgets.QProgressBar(self.statusbar)
        self.progress_bar.setRange(0, 100)
        self.progress_bar.setValue(0)
        self.progress_bar.hide()
        self.statusbar_right = QtWidgets.QLabel(self.statusbar)
        self.statusbar.addPermanentWidget(self.progress_bar)
        self.statusbar.addPermanentWidget(self.statusbar_right)

        self.media_player: Optional[QMediaPlayer] = None

        self.action_quit.triggered.connect(self.close)
        self.action_file_prev.triggered.connect(self.open_prev_file)
        self.action_file_next.triggered.connect(self.open_next_file)
        self.action_file_first.triggered.connect(self.open_first_file)
        self.action_file_last.triggered.connect(self.open_last_file)
        self.action_open.triggered.connect(self.open_file)
        self.action_about.triggered.connect(self.about)
        self.action_file_info.triggered.connect(self.file_info)
        self.action_reload.triggered.connect(self.reload)

        self.action_toggle_font.triggered.connect(self.toggle_font)
        self.action_view_as_binary.triggered.connect(self.reload_as_binary)
        self.action_view_as_normal.triggered.connect(self.reload)
        self.action_view_plain_text.triggered.connect(self.reload_as_plain_text)

        self.action_play_pause.triggered.connect(self.play_toggle)
        self.action_mute_audio.triggered.connect(self.mute_toggle)
        self.action_fwd_small.triggered.connect(self.fwd_small)
        self.action_fwd_large.triggered.connect(self.fwd_large)
        self.action_back_small.triggered.connect(self.back_small)
        self.action_back_large.triggered.connect(self.back_large)

        self.action_page_first.triggered.connect(self.document_page_first)
        self.action_page_last.triggered.connect(self.document_page_last)
        self.action_page_prev.triggered.connect(self.document_page_prev)
        self.action_page_next.triggered.connect(self.document_page_next)

        self.action_scale_to_window.triggered.connect(self.toggle_scale)
        self.action_fullscreen.triggered.connect(self.toggle_fullscreen)

        self.media_slider.valueChanged['int'].connect(self.media_seek_changed)
        self.media_slider.sliderPressed.connect(self.media_seek_pressed)
        self.media_slider.sliderReleased.connect(self.media_seek_released)
        self.manual_seek: bool = False

        self.rtext_view.anchorClicked['QUrl'].connect(self.on_link_clicked)
        self.rtext_view.highlighted['QUrl'].connect(self.on_link_highlighted)
        # TODO: Doesn't work:
        # self.action_history_back.triggered.connect(self.rtext_view.backward)
        # self.action_history_forward.triggered.connect(self.rtext_view.forward)


        # Find
        self.find_panel.hide()
        self.action_find.triggered.connect(self.open_find_panel)
        self.action_find_next.triggered.connect(self.find_next)
        self.text_find.textChanged.connect(self.find_text_changed)
        self.but_find_next.clicked.connect(self.find_next)
        self.but_find_close.clicked.connect(self.find_panel.hide)

        # Additional shortcuts
        fn1: QShortcut = QShortcut(QKeySequence("F3"), self)
        fn1.activated.connect(self.find_next)
        fn2: QShortcut = QShortcut(QKeySequence("Ctrl-N"), self)
        fn2.activated.connect(self.find_next)

        # Use a timer to prevent searching on every textChanged event
        self.find_timer: QTimer = QTimer(self)
        self.find_timer.setSingleShot(True)
        self.find_timer.timeout.connect(self.find_text)


        self.file_browsing_modes: List = [
            DISPLAY_IMAGE,
        ]
        self.page_browsing_modes: List = [
            DISPLAY_DOCUMENT,
        ]

        self.media_controls = [
            self.action_mute_audio,
            self.action_play_pause,
            self.action_fwd_small,
            self.action_fwd_large,
            self.action_back_small,
            self.action_back_large,
        ]

        self.image_controls = [
            self.action_scale_to_window,
            self.action_file_first,
            self.action_file_last,
            self.action_file_prev,
            self.action_file_next,
        ]

        self.document_controls = [
            self.action_scale_to_window,
            self.action_page_first,
            self.action_page_last,
            self.action_page_prev,
            self.action_page_next,
        ]

        self.text_controls = [
            self.action_toggle_font,
            self.action_view_as_binary,
            self.action_view_as_normal,
            self.action_view_plain_text,
            self.action_history_back,
            self.action_history_forward,
            self.action_find,
        ]

        self.current_filename: str = None
        self.old_cwd: Optional[str] = None
        self.filelist: list = []
        self.browser = "sensible-browser"

        self.restore_config()

        self.font_mono: QFont = QFont('Monospace')
        self.font_vari: QFont = QFont('Sans')
        self.font: QFont = self.font_mono
        self.text_view.setFont(self.font)
        self.rtext_view.setFont(self.font)
        self.compression: str = None
        self.text_encoding: str = None
        self.mime_type: str = None
        self.plugin: PluginBase = None
        self.plugin_instance: PluginBase = None
        self.document_page: int = 1
        self.media_duration: int = 0
        self.media_pos: int = 0
        self.old_window_state = self.windowState()

        self.init_controls()


    ### Event handlers

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls():
            event.acceptProposedAction();

    def dropEvent(self, event):
        if event.mimeData().hasUrls():
            file_list: List = []
            for url in event.mimeData().urls():
                filename: str = url.toLocalFile()
                if filename != "":
                    self.load_file(filename)
                    return # only load the first file

    def keyPressEvent(self, event):
        if event.modifiers() == QtCore.Qt.ControlModifier:
            if event.key() == QtCore.Qt.Key_W:
                self.close()
        elif event.key() == QtCore.Qt.Key_Escape and self.text_find.hasFocus():
            self.find_panel.hide()
        elif event.key() == QtCore.Qt.Key_Escape:
            self.action_stop_threads.trigger()

    def resizeEvent(self, event):
        if self.stacked_widget.currentWidget() == self.image_page:
            self.rescale_image()

    def closeEvent(self, event):
        self.save_config()
        self.save_plugin_config()
        self.action_stop_threads.trigger()

    def progress(self, value: int):
        self.progress_bar.setValue(value)


    ## Config

    def save_config(self):
        settings = QSettings()

        # Window settings. Only save default if no plugin is active
        # if not self.plugin_instance:
        #     settings.beginGroup("MainWindow")
        #     settings.setValue("Size", self.size());
        #     settings.setValue("Pos", self.pos());
        #     settings.endGroup()

        # Keyboard shortcuts
        settings.beginGroup("KeyboardShortcuts")
        for action in self.actions():
            name = action.objectName()
            key = action.shortcut().toString()
            if name:
                settings.setValue(name, key);
        settings.endGroup()


    def restore_config(self):
        settings = QSettings()

        # Window settings
        # settings.beginGroup("MainWindow")

        # pos = settings.value("Pos")
        # if pos: self.move(pos)

        # size = settings.value("Size")
        # if size: self.resize(size)

        # settings.endGroup()

        # Keyboard shortcuts
        settings.beginGroup("KeyboardShortcuts")
        for action in self.actions():
            name = action.objectName()
            if name:
                key = settings.value(name)
                try:
                    action.setShortcut(key)
                except Exception as e:
                    print(f"Invalid keyboard shortcut {key} for {name}")
        settings.endGroup()

        # General settings (no begin/endGroup() needed)
        browser: str = settings.value("WebBrowser")
        if browser: self.browser = browser


    def save_plugin_config(self):
        if not self.plugin_instance:
            return
        display = self.plugin_instance.get_display()

        settings = QSettings()

        settings.beginGroup("Plugin_"+display)
        # settings.setValue("Size", self.size());
        # settings.setValue("Pos", self.pos());
        settings.endGroup()
        settings.sync()


    def restore_plugin_config(self):
        if not self.plugin_instance:
            return
        display = self.plugin_instance.get_display()

        settings = QSettings()

        settings.beginGroup("Plugin_"+display)

        # pos = settings.value("Pos")
        # if pos: self.move(pos)

        # size = settings.value("Size")
        # if size: self.resize(size)

        settings.endGroup()


    ### Functions

    def progress_pending(self, pending: bool):
        if pending:
            self.progress_bar.setRange(0, 0)
        else:
            self.progress_bar.setRange(0, 100)


    def on_link_clicked(self, qurl: QUrl):
        url: str = qurl.toString()

        # Handle '#'-links
        if url.startswith("#"):
            url = f"{self.current_filename}{url}"
            self.rtext_view.setSource(QUrl(url))
            return
        elif url.startswith(f"{self.current_filename}#"):
            self.rtext_view.setSource(QUrl(url))
            return

        if not url.startswith("http"):
            self.statusbar.showMessage(
                QApplication.translate("Omniview", "Invalid uri")
            )
            return

        try:
            command: List = build_command(self.browser, url)
            subprocess.Popen(command)
            self.statusbar.showMessage(
                QApplication.translate("Omniview", "Started browser")
            )
        except Exception as e:
            msg: str = QApplication.translate(
                "Omniview", "Failed to start browser: {0}"
            ).format(self.browser)
            self.statusbar.showMessage(msg)
            QMessageBox.information(
                self,
                QApplication.translate("Omniview", "Error"),
                msg
            )


    def on_link_highlighted(self, url: QUrl):
        self.statusbar.showMessage(url.toString())


    def toggle_scale(self):
        if self.stacked_widget.currentWidget() == self.image_page:
            self.rescale_image()

    def toggle_fullscreen(self):
        # TODO: hide slider when when mouse is not hovering over bottom of window
        # TODO: hide controls in fullscreen (also restore the correct controls
        # when exiting fullscreen)
        current_window_state = self.windowState()
        if current_window_state & QtCore.Qt.WindowFullScreen:
            self.menubar.show()
            self.statusbar.show()
            self.setWindowState(self.old_window_state)
        else:
            self.menubar.hide()
            self.statusbar.hide()
            self.setWindowState(QtCore.Qt.WindowFullScreen)

        self.old_window_state = current_window_state

    def reload_as_binary(self):
        self.load_file(self.current_filename, 'application/octet-stream')

    def reload_as_plain_text(self):
        self.load_file(self.current_filename, 'text/plain')

    def reload(self):
        self.load_file(self.current_filename)

    def about(self):
        AboutDialog(self).exec()

    def file_info(self):
        InfoDialog(self, self.current_filename).exec()

    def init_controls(self):
        display: str = DISPLAY_PLAIN_TEXT
        if self.plugin_instance:
            display = self.plugin_instance.get_display()

        self.find_panel.hide()

        if display != DISPLAY_IMAGE:
            self.toolbar_image.hide()
            for w in self.image_controls:
                w.setDisabled(True)

        if display != DISPLAY_DOCUMENT:
            self.toolbar_document.hide()
            for w in self.document_controls:
                w.setDisabled(True)

        if display != DISPLAY_VIDEO and display != DISPLAY_AUDIO:
            self.toolbar_media.hide()
            for w in self.media_controls:
                w.setDisabled(True)

        if display != DISPLAY_PLAIN_TEXT:
            self.toolbar_text.hide()
            for w in self.text_controls:
                w.setDisabled(True)

        if display != DISPLAY_RICH_TEXT:
            self.toolbar_text.hide()
            for w in self.text_controls:
                w.setDisabled(True)


        if display == DISPLAY_IMAGE:
            self.toolbar_image.show()
            for w in self.image_controls:
                w.setDisabled(False)

        elif display == DISPLAY_DOCUMENT:
            self.toolbar_document.show()
            for w in self.document_controls:
                w.setDisabled(False)

        elif display == DISPLAY_VIDEO or display == DISPLAY_AUDIO:
            self.toolbar_media.show()
            for w in self.media_controls:
                w.setDisabled(False)

        elif display == DISPLAY_RICH_TEXT:
            self.toolbar_text.show()
            for w in self.text_controls:
                w.setDisabled(False)

        else:
            self.toolbar_text.show()
            for w in self.text_controls:
                w.setDisabled(False)

        self.init_shortcuts(display)


    def init_shortcuts(self, display: str):
        disabled: Qt.ShortcutContext = Qt.WidgetShortcut # type: ignore
        enabled: Qt.ShortcutContext = Qt.WindowShortcut # type: ignore

        if display in self.file_browsing_modes:
            self.action_file_next.setShortcutContext(enabled)
            self.action_file_prev.setShortcutContext(enabled)
            self.action_file_first.setShortcutContext(enabled)
            self.action_file_last.setShortcutContext(enabled)

            self.action_page_next.setShortcutContext(disabled)
            self.action_page_prev.setShortcutContext(disabled)
            self.action_page_last.setShortcutContext(disabled)
            self.action_page_first.setShortcutContext(disabled)

        elif display in self.page_browsing_modes:
            self.action_page_next.setShortcutContext(enabled)
            self.action_page_prev.setShortcutContext(enabled)
            self.action_page_last.setShortcutContext(enabled)
            self.action_page_first.setShortcutContext(enabled)

            self.action_file_next.setShortcutContext(disabled)
            self.action_file_prev.setShortcutContext(disabled)
            self.action_file_first.setShortcutContext(disabled)
            self.action_file_last.setShortcutContext(disabled)

        else:
            self.action_page_next.setShortcutContext(disabled)
            self.action_page_prev.setShortcutContext(disabled)
            self.action_page_last.setShortcutContext(disabled)
            self.action_page_first.setShortcutContext(disabled)

            self.action_file_next.setShortcutContext(disabled)
            self.action_file_prev.setShortcutContext(disabled)
            self.action_file_first.setShortcutContext(disabled)
            self.action_file_last.setShortcutContext(disabled)



    def init_view(self):
        display: str = DISPLAY_PLAIN_TEXT
        if self.plugin_instance:
            display = self.plugin_instance.get_display()

        if display != DISPLAY_IMAGE and display != DISPLAY_DOCUMENT:
            self.clear_image()

        if display != DISPLAY_VIDEO and display != DISPLAY_AUDIO:
            self.media_player = None

        if display != DISPLAY_PLAIN_TEXT:
            self.text_view.clear()

        if display != DISPLAY_RICH_TEXT:
            self.rtext_view.clear()


        if display == DISPLAY_IMAGE:
            self.stacked_widget.setCurrentWidget(self.image_page)

        elif display == DISPLAY_DOCUMENT:
            self.stacked_widget.setCurrentWidget(self.image_page)

        elif display == DISPLAY_VIDEO or display == DISPLAY_AUDIO:
            self.stacked_widget.setCurrentWidget(self.video_page)

        elif display == DISPLAY_RICH_TEXT:
            self.stacked_widget.setCurrentWidget(self.rtext_page)

        else:
            self.stacked_widget.setCurrentWidget(self.text_page)

        self.init_controls()
        self.restore_plugin_config()


    def toggle_font(self):
        if self.font == self.font_mono:
            self.font = self.font_vari
        else:
            self.font = self.font_mono
        self.text_view.setFont(self.font)
        self.rtext_view.setFont(self.font)


    # Select font based on mime type
    def autoselect_font(self, mime_type: str):
        if mime_type is None:
            return

        fonts = {
            'text/plain':       self.font_mono,
            'text/html':        self.font_vari,
            'text/markdown':    self.font_vari,
            'text/.*':          self.font_mono,
            'application/.*':   self.font_mono,
        }

        for pattern in fonts:
            regex = re.compile(pattern, re.I)
            if regex.match(mime_type):
                self.font = fonts[pattern]
                break

        self.text_view.setFont(self.font)
        self.rtext_view.setFont(self.font)


    def open_file(self):
        self.pause_media()

        self.save_plugin_config()

        start_dir: str = None
        if self.current_filename:
            start_dir = os.path.dirname(self.current_filename)
        else:
            start_dir = os.environ["HOME"]

        filename, _ = QFileDialog.getOpenFileName(
            self,
            QApplication.translate("Omniview", "Open File"),
            start_dir
        )
        self.load_file(filename)


    # Searching

    def open_find_panel(self, toggled):
        if not self.is_text_display: return

        if not self.find_panel.isVisible():
            self.find_panel.setVisible(True)
        self.text_find.setFocus()
        self.text_find.selectAll()


    def find_text_changed(self):
        if not self.is_text_display: return
        # Don't call find action on every textChanged event
        self.find_timer.start(200) # ms


    # TODO: maybe use QTextDocument::FindFlags
    def find_text(self):
        if not self.is_text_display: return

        search_for: str = self.text_find.text()

        text_view: QTextEdit = self.text_view
        if self.plugin_instance.get_display() == DISPLAY_RICH_TEXT:
            text_view = self.rtext_view

        cursor: QTextCursor = text_view.textCursor()

        if text_view.find(search_for):
            self.statusbar.showMessage(
                QApplication.translate("Omniview", "Found")
            )
        else:
            # Not found, move the cursor to the start of document
            cursor.movePosition(QTextCursor.Start)
            text_view.setTextCursor(cursor)

            # Try again from the beginning
            if text_view.find(search_for):
                self.statusbar.showMessage(
                    QApplication.translate("Omniview", "Found")
                )
            else:
                self.statusbar.showMessage(
                    QApplication.translate("Omniview", "Not found")
                )


    def find_next(self):
        if not self.is_text_display: return

        if not self.find_panel.isVisible():
            self.find_panel.setVisible(True)
            self.text_find.setFocus()
            self.text_find.selectAll()

        text_view: QTextEdit = self.text_view
        if self.plugin_instance.get_display() == DISPLAY_RICH_TEXT:
            text_view = self.rtext_view

        cursor: QTextCursor = text_view.textCursor()
        cursor.movePosition(QTextCursor.NextCharacter)
        self.find_text()


    def is_text_display(self) -> bool:
        if not self.plugin_instance: return False

        display: str = self.plugin_instance.get_display()
        return display == DISPLAY_RICH_TEXT or display == DISPLAY_PLAIN_TEXT


    # File navigation

    def get_file_number(self) -> Optional[int]:
        if not self.is_in_file_browse_mode():
            return None

        for i in range(len(self.filelist)):
            if self.filelist[i] == self.current_filename:
                return i

        return None


    def get_number_of_files(self) -> Optional[int]:
        if not self.is_in_file_browse_mode():
            return None
        return len(self.filelist)


    def open_prev_file(self):
        self.open_next_or_prev_file(-1)

    def open_next_file(self):
        self.open_next_or_prev_file(1)

    def open_next_or_prev_file(self, direction: int):
        if not self.is_in_file_browse_mode():
            return

        target: Optional[str] = None
        numitems: int = len(self.filelist)

        for i in range(numitems):
            if not os.path.isfile(self.filelist[i]):
                self.load_filelist(True)
                break

        numitems = len(self.filelist)
        for i in range(numitems):
            next_i: int = i + direction
            if (self.filelist[i] == self.current_filename and
                next_i >= 0 and
                next_i < numitems
            ):
                if not os.path.isfile(self.filelist[next_i]):
                    continue
                target = self.filelist[next_i]

        if target:
            self.load_file(target)


    def open_first_file(self):
        if not self.is_in_file_browse_mode():
            return

        target = self.filelist[0]

        if not os.path.isfile(target):
            self.load_filelist(True)
            target = self.filelist[0]

        if target and target != self.current_filename:
            self.load_file(target)

    def open_last_file(self):
        if not self.is_in_file_browse_mode():
            return

        target = self.filelist[len(self.filelist) - 1]

        if not os.path.isfile(target):
            self.load_filelist(True)
            target = self.filelist[len(self.filelist) - 1]

        if target and target != self.current_filename:
            self.load_file(target)


    def is_in_file_browse_mode(self) -> bool:
        # if not self.mime_type:
        #     return False
        # if self.mime_type.split('/')[0] != 'image':
        #     return False
        # return True
        if self.plugin_instance:
            display = self.plugin_instance.get_display()
            if display in self.file_browsing_modes:
                return True
        return False


    def load_filelist(self, force: bool):
        if not self.mime_type:
            return

        if not self.is_in_file_browse_mode():
            return

        file_type: str = self.mime_type.split('/')[0]
        file_and_path: str = os.path.realpath(self.current_filename)
        cwd: str = os.path.dirname(file_and_path)
        if not force:
            if cwd == self.old_cwd and len(self.filelist) != 0:
                return

        self.progress_bar.setValue(0)
        self.progress_bar.show()

        files: list = []
        dir_contents: List = os.listdir(cwd)
        num_total: int = len(dir_contents)
        idx: int = 0
        for f in dir_contents:
            f_full = os.path.join(cwd, f)

            if not os.path.isfile(f_full):
                continue

            if f.startswith('.'):
                continue

            try:
                type = get_mimetype(f_full)
            except:
                continue
            if type['mime_type'].split('/')[0] != file_type:
                continue

            files.append(f_full)
            self.progress_bar.setValue(int((idx + 1) / num_total * 100))
            idx += 1

        files.sort(key = lambda s: s.casefold())
        self.filelist = files
        self.old_cwd = os.path.dirname(file_and_path)
        self.show_brief_info()

        self.progress_bar.setValue(100)
        self.progress_bar.hide()


    def load_filelist_no_force(self):
        self.load_filelist(False)


    # File loading

    def load_file(self, filename: str, force_type: Optional[str] = None):
        if not filename:
            return

        # Add full path to filename if it's relative
        filename = str(Path(filename).absolute())

        if not os.path.exists(filename):
            message = QApplication.translate(
                "Omniview", "File does not exist: {0}"
            ).format(filename)
            QMessageBox.information(
                self,
                QApplication.translate("Omniview", "Error"),
                message
            )
            return

        if not os.path.isfile(filename):
            message = QApplication.translate(
                "Omniview", "Not a file: {0}"
            ).format(filename)
            QMessageBox.information(
                self,
                QApplication.translate("Omniview", "Error"),
                message
            )
            return

        if not os.access(filename, os.R_OK):
            message = QApplication.translate(
                "Omniview", "Permission denied: {0}"
            ).format(filename)
            QMessageBox.information(
                self,
                QApplication.translate("Omniview", "Error"),
                message
            )
            return

        mime_type: str = ''
        compression: Optional[str] = None
        if force_type:
            mime_type = force_type
            debug_print(f"Info: forced mime-type: {mime_type}")
        else:
            mime_type_data = get_mimetype(filename)
            mime_type = mime_type_data['mime_type']
            compression = mime_type_data['compression']
            debug_print(f"Info: detected mime-type: {mime_type}, compression: {compression}")

        message = QApplication.translate(
            "Omniview", "Loading file, please wait ..."
        )
        self.statusbar.showMessage(message)

        try:
            plugin = find_plugin(mime_type)
        except RuntimeWarning as e:
            message = QApplication.translate(
                "Omniview", str(e)
            )
            QMessageBox.information(
                self,
                QApplication.translate("Omniview", "Error"),
                message
            )
            message = QApplication.translate(
                "Omniview", "Loading file failed"
            )
            self.statusbar.showMessage(message)
            return

        if compression and not plugin.supports_compression():
            message = QApplication.translate(
                "Omniview", "Loader for {0} does not support compressed files"
            ).format(mime_type)
            QMessageBox.information(
                self,
                QApplication.translate("Omniview", "Error"),
                message
            )
            self.statusbar.showMessage(message)
            return

        self.progress_bar.setEnabled(True)
        self.progress_bar.setRange(0, 100)
        self.progress_bar.show()

        plugin.signals.progress.connect(self.progress_bar.setValue) # type: ignore

        load_file_worker = LoadFileWorker(
            filename,
            mime_type,
            compression,
            plugin
        )

        load_file_worker.signals.error.connect(self.on_load_error) # type: ignore
        load_file_worker.signals.result.connect(self.on_load_result) # type: ignore
        load_file_worker.signals.status.connect(self.statusbar.showMessage) # type: ignore
        load_file_worker.signals.progress.connect(self.progress_bar.setValue) # type: ignore

        self.action_stop_threads.triggered.connect(load_file_worker.stop)
        self.action_stop_threads.triggered.connect(plugin.stop)

        self.threadpool.start(load_file_worker)


    @pyqtSlot(bytes, str, str, str, str, PluginBase)
    def on_load_result(
        self,
        data,
        filename: str,
        mime_type: str,
        compression: str,
        text_encoding: str,
        plugin: PluginBase
    ):
        try:
            display_enabled = self.display_loaded_data(
                data,
                filename,
                plugin
            )
        except RuntimeWarning as e:
            message = QApplication.translate(
                "Omniview", str(e)
            )
            QMessageBox.information(
                self,
                QApplication.translate("Omniview", "Error"),
                message
            )
            self.statusbar.showMessage(message)
            return
        except ModuleNotFoundError as e:
            message = QApplication.translate(
                "Omniview", "Failed to load plugin: {0}"
            ).format(str(e))
            QMessageBox.information(
                self,
                QApplication.translate("Omniview", "Error"),
                message
            )
            self.statusbar.showMessage(message)
            return
        except (UnicodeDecodeError, LookupError) as e:
            message = QApplication.translate(
                "Omniview", "Failed to decode data"
            )
            QMessageBox.information(
                self,
                QApplication.translate("Omniview", "Error"),
                message
            )
            self.statusbar.showMessage(message)
            return

        if (display_enabled == DISPLAY_PLAIN_TEXT or
            display_enabled == DISPLAY_RICH_TEXT
        ):
            self.autoselect_font(mime_type)

        self.init_view()

        self.mime_type = mime_type
        self.current_filename = filename
        self.compression = compression
        self.text_encoding = text_encoding

        display_dir = os.path.dirname(self.current_filename)
        display_file = os.path.basename(self.current_filename)
        self.setWindowTitle(
            f"Omniview - {display_file} in {display_dir}"
        )
        message = QApplication.translate(
            "Omniview", "File loaded. Type: {0}"
        ).format(mime_type)
        self.statusbar.showMessage(message)
        self.show_brief_info()

        self.progress_bar.setValue(0)
        self.progress_bar.hide()

        # Load filelist in the background
        QTimer.singleShot(0, self.load_filelist_no_force)


    def display_loaded_data(self, data, filename: str, plugin: PluginBase) -> str:
        self.progress_bar.setRange(0, 0)
        try:
            self.document_page = 1
            self.plugin_instance = plugin
            display = plugin.get_display()

            if display == DISPLAY_RICH_TEXT:
                self.rtext_view.setHtml(data.decode("utf-8"))

            elif display == DISPLAY_PLAIN_TEXT:
                self.text_view.setPlainText(data.decode("utf-8"))

            elif display == DISPLAY_IMAGE:
                self.display_image(data)

            elif display == DISPLAY_DOCUMENT:
                self.display_image(data)
                # Cache pages in the background
                QTimer.singleShot(0, self.cache_pages)

            elif display == DISPLAY_VIDEO:
                self.display_video(filename)

            elif display == DISPLAY_AUDIO:
                self.display_video(filename)

        except Exception as e:
            raise e

        return display


    @pyqtSlot(str)
    def on_load_error(self, message: str):
        self.statusbar.showMessage(message)
        self.progress_bar.setValue(0)
        self.progress_bar.hide()


    def show_brief_info(self):
        if not self.plugin_instance:
            return

        info: str = ""
        display: str = self.plugin_instance.get_display()

        if display == DISPLAY_DOCUMENT:
            num_pages: int = self.plugin_instance.get_num_pages()
            info = f"page {self.document_page} of {num_pages}"

        elif display == DISPLAY_IMAGE:
            items = self.image_view.items()
            if len(items) > 0:
                pixmap_item = items[0]
                pixmap = pixmap_item.pixmap()
                file_num: Optional[int] = self.get_file_number()
                num_files: Optional[int] = self.get_number_of_files()
                if file_num is not None and num_files is not None:
                    info = f"File %d of %d, %d x %d px" % (
                        file_num + 1,
                        num_files,
                        pixmap.width(),
                        pixmap.height()
                    )
                else:
                    info = f"%d x %d px" % (
                        pixmap.width(),
                        pixmap.height()
                    )

        elif display == DISPLAY_PLAIN_TEXT:
            num_lines = self.text_view.toPlainText().count('\n')
            if num_lines == 1:
                info = f"{num_lines} line"
            else:
                info = f"{num_lines} lines"

        elif display == DISPLAY_AUDIO or display == DISPLAY_VIDEO:
            dur_sec: int = round(self.media_duration / 1000)
            pos_sec: int = round(self.media_pos / 1000)
            fmt: str = "%M:%S"
            if dur_sec > 3600:
                fmt = "%H:%M:%S"
            t_dur: str = time.strftime(fmt, time.gmtime(dur_sec))
            t_pos: str = time.strftime(fmt, time.gmtime(pos_sec))
            info = f"{t_pos} / {t_dur}"


        if info == "":
            self.statusbar_right.hide()
        else:
            self.statusbar_right.setText(info)



    # Multipage documents

    def document_page_first(self):
        if not self.plugin_instance:
            return
        self.go_to_page(1)

    def document_page_last(self):
        if not self.plugin_instance:
            return
        self.go_to_page(self.plugin_instance.get_num_pages())

    def go_to_page(self, new_page: int):
        try:
            self.display_image(
                self.plugin_instance.get_processed_data({'page_num': new_page})
            )
        except (RuntimeWarning, ValueError) as e:
            self.statusbar.showMessage(str(e))

        self.document_page = new_page
        self.show_brief_info()
        QTimer.singleShot(0, self.cache_pages)

    def document_page_prev(self):
        self.turn_page(-1)

    def document_page_next(self):
        self.turn_page(1)


    def cache_pages(self):
        try:
            cur_page: int = self.document_page
            last_page: int = self.plugin_instance.get_num_pages()

            pages_to_cache: List[int] = [
                cur_page + 1,
                cur_page - 1,
                cur_page + 2,
                cur_page + 3,
            ]

            for page in pages_to_cache:
                if 1 < page < last_page:
                    self.plugin_instance.get_processed_data({'page_num': page})

        except (RuntimeWarning, ValueError) as e:
            self.statusbar.showMessage(str(e))


    def turn_page(self, pages: int):
        if not self.plugin_instance:
            return

        new_page: int = self.document_page + pages
        last_page: int = self.plugin_instance.get_num_pages()

        if new_page < 1 or new_page > last_page:
            return

        self.go_to_page(new_page)


    # Images

    def display_image(self, data):
        try:
            im: PILImage = PILImage.open(io.BytesIO(data))
            image_data = ImageQt.ImageQt(im.convert('RGBA'))
        except IOError as e:
            message = QApplication.translate(
                "Omniview", "failed to load image: {0}"
            ).format(str(e))
            raise RuntimeWarning(message)

        scene = QtWidgets.QGraphicsScene()
        self.image_view.setScene(scene)
        scene.addPixmap(QtGui.QPixmap.fromImage(image_data))
        self.rescale_image()


    def rescale_image(self):
        self.image_view.resetTransform()
        if self.action_scale_to_window.isChecked():
            items = self.image_view.items()
            if len(items) <= 0:
                return
            pixmap_item = items[0]
            pixmap_item.setTransformationMode(Qt.SmoothTransformation)
            pixmap = pixmap_item.pixmap()

            scale_w: float = self.image_view.width() / pixmap.width()
            if scale_w > 1:
                scale_w = 1.0
            scale_h: float = self.image_view.height() / pixmap.height()
            if scale_h > 1:
                scale_h = 1.0
            scale: float = min(scale_h, scale_w)

            self.image_view.scale(scale, scale)
        else:
            self.image_view.scale(1.0, 1.0)


    def clear_image(self):
        scene: QtWidgets.QGraphicsScene = QtWidgets.QGraphicsScene()
        self.image_view.setScene(scene)


    # Video / audio

    def display_video(self, filename: str):
        self.media_player = QMediaPlayer(None, QMediaPlayer.VideoSurface) # type: ignore
        self.media_player.setVideoOutput(self.video_view)
        self.media_player.setMedia(QMediaContent(QUrl.fromLocalFile(filename)))

        self.media_duration = 0
        self.media_pos = 0
        self.media_player.durationChanged['qint64'].connect(self.media_duration_changed) # type: ignore
        self.media_player.positionChanged['qint64'].connect(self.media_pos_changed) # type: ignore

        self.media_player.play()
        self.action_play_pause.setChecked(not self.media_paused_or_stopped())
        self.action_mute_audio.setChecked(self.media_player.isMuted())

    # Media duration / position and seeking

    def media_duration_changed(self, value: int):
        self.media_duration = value
        self.media_slider.setMaximum(value)
        self.show_brief_info()

    def media_pos_changed(self, value: int):
        if not self.manual_seek:
            self.media_slider.setValue(value)
            # TODO: does not work as it should
            # if value >= self.media_duration:
            #     self.stop()
        self.media_pos = value
        self.show_brief_info()

    def media_seek_changed(self, value: int):
        if self.manual_seek and self.media_player:
            self.media_player.setPosition(value)

    def media_seek_pressed(self):
        self.manual_seek = True

    def media_seek_released(self):
        self.manual_seek = False


    def adj_media_pos(self, value: int):
        if not self.media_player:
            return
        self.media_player.setPosition(clamp(
            self.media_player.position() + value,
            0,
            self.media_duration
        ))

    def fwd_small(self):
        if self.media_player_active():
            self.adj_media_pos(5000)

    def fwd_large(self):
        if self.media_player_active():
            self.adj_media_pos(60000)

    def back_small(self):
        if self.media_player_active():
            self.adj_media_pos(-5000)

    def back_large(self):
        if self.media_player_active():
            self.adj_media_pos(-60000)

    def media_player_active(self) -> bool:
        return self.stacked_widget.currentWidget() == self.video_page

    # Media controls

    def media_paused_or_stopped(self) -> bool:
        if not self.media_player:
            return False
        stopped_states = [QMediaPlayer.StoppedState, QMediaPlayer.PausedState] # type: ignore
        return (self.media_player.state() in stopped_states)

    def pause_media(self):
        if not self.media_player_active():
            return
        self.media_player.pause()
        self.action_play_pause.setChecked(not self.media_paused_or_stopped())

    def play_toggle(self):
        if not self.media_player_active():
            return

        if self.media_paused_or_stopped():
            self.media_player.play()
            message = ""
        else:
            self.media_player.pause()
            message = QApplication.translate("Omniview", "Paused")

        self.action_play_pause.setChecked(not self.media_paused_or_stopped())
        self.statusbar.showMessage(message)

    def mute_toggle(self):
        if not self.media_player_active():
            return

        self.media_player.setMuted(not self.media_player.isMuted())

        if self.media_player.isMuted():
            message = QApplication.translate("Omniview", "Muted")
        else:
            message = ""

        self.action_mute_audio.setChecked(self.media_player.isMuted())
        self.statusbar.showMessage(message)

