import chardet
import codecs
import importlib
import magic
import mimetypes
import os
import pkgutil
from typing import Dict, List, Optional, Tuple
from omniview.plugin_base import PluginBase

DISPLAY_PLAIN_TEXT: str = 'text'
DISPLAY_RICH_TEXT: str = 'rtext'
DISPLAY_IMAGE: str = 'image'
DISPLAY_AUDIO: str = 'audio'
DISPLAY_VIDEO: str = 'video'
DISPLAY_DOCUMENT: str = 'document'


def find_plugin(mime_type: Optional[str]) -> PluginBase:
    if mime_type is None:
        mime_type = 'application/octet-stream'

    plugins_primary: List = []
    plugins_secondary: List = []
    plugins_path: str = os.path.join(os.path.dirname(__file__), "plugins")
    for loader, mod_name, ispkg in pkgutil.iter_modules([plugins_path], "omniview.plugins."):
        try:
            m = importlib.import_module(mod_name)
        except ModuleNotFoundError as e:
            # TODO: Better information about missing dependencies?
            print("Module failed to load "+mod_name+": "+str(e))
        else:
            p = m.Plugin() # type: ignore
            if p.matches_mime_type(mime_type):
                if p.prio() == 1:
                    plugins_primary.append(p)
                else:
                    plugins_secondary.append(p)

    selected_plugin = None
    for p in plugins_primary:
        selected_plugin = p

    if not selected_plugin:
        for p in plugins_secondary:
            selected_plugin = p

    if not selected_plugin:
        raise RuntimeWarning(f"No plugin for file type yet: {mime_type}")

    return selected_plugin


# return Dict{'mime_type': str, 'compression': str|None}
# If the file is compressed, return mime-type of the file inside
# the compressed container, and the compression format.
# Otherwise return the mime-type of the file, and None as compression format
def get_mimetype(filename: str) -> Dict:
    filename = os.path.realpath(filename)
    m: Tuple[Optional[str], Optional[str]] = mimetypes.guess_type(filename)
    mime_type: Optional[str] = m[0]
    compression: Optional[str] = m[1]

    if mime_type is None:
        mime: magic.Magic = magic.Magic(mime=True, uncompress=True)
        mime_type = mime.from_file(filename)

    return {
        'mime_type':    mime_type,
        'compression':  compression,
    }


def clamp(value: int, low_limit: int, hi_limit: int) -> int:
    if value > hi_limit:
        return hi_limit
    elif value < low_limit:
        return low_limit
    else:
        return value


def debug_print(text: str):
    if "OMNIVIEW_DEBUG" in os.environ:
        print(text)


def decode_text(data: bytes) -> Tuple[str, str]:
    try:
        return (
            data.decode("utf-8"),
            "utf-8"
        )
    except UnicodeDecodeError as e:
        # Handle non-utf8 encodings
        enc: str = detect_encoding(data)
        return (
            codecs.decode(data, encoding=enc, errors="replace"),
            enc
        )


def detect_encoding(data: bytes) -> str:
    detected = chardet.detect(data)
    enc: str = detected["encoding"]

    if enc is None:
        debug_print(f"Failed to detect text encoding {enc}, trying ascii")
        enc = "ascii"
    else:
        try:
            codecs.lookup(enc)
            debug_print(f"Detected text encoding: {enc}")
        except LookupError as e:
            debug_print(f"Failed to detect text encoding {enc}, trying ascii")
            enc = "ascii"

    return enc


def format_size(bytes_size: int) -> str:
    suffixes = ["B", "KiB", "MiB", "GiB", "TiB"]
    ac: float = float(bytes_size)

    for suf in suffixes:
        if ac < 1024.0:
            if suf == "B":
                return f"{ac:.0f} {suf}"
            else:
                return f"{ac:.0f} {suf}"
        ac /= 1024.0

    return f"{ac:.0f} PiB"


def build_command(command_tpl: str, arg: str) -> List:
    command_list: List = []
    replaced: bool = False

    for s in command_tpl.split(" "):
        if s == "%u" and not replaced:
            command_list.append(arg)
            replaced = True
        else:
            command_list.append(s)

    if not replaced:
        command_list.append(arg)

    return command_list
