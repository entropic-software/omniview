import inspect
from typing import Dict, Callable, Optional
from PyQt5.QtCore import QObject, pyqtSignal, pyqtSlot

class PluginSignals(QObject):
    progress = pyqtSignal(int)

class PluginBase:
    def __init__(self):
        self.signals: PluginSignals = PluginSignals()
        self.interrupted: bool = False

    def prio(self) -> int:
        raise NotImplementedError("Implement "+type(self).__name__+"."+inspect.stack()[0][3])

    def needs_data(self) -> bool:
        raise NotImplementedError("Implement "+type(self).__name__+"."+inspect.stack()[0][3])

    def supports_compression(self) -> bool:
        return True

    def matches_mime_type(self, t: str) -> bool:
        raise NotImplementedError("Implement "+type(self).__name__+"."+inspect.stack()[0][3])

    def set_data(self, data: bytes, filename: str):
        self.data: bytes = data
        self.filename: str = filename

    def get_display(self) -> str:
        raise NotImplementedError("Implement "+type(self).__name__+"."+inspect.stack()[0][3])

    def get_processed_data(self, options: Dict = {}):
        raise NotImplementedError("Implement "+type(self).__name__+"."+inspect.stack()[0][3])

    def get_num_pages(self) -> int:
        raise NotImplementedError("Implement "+type(self).__name__+"."+inspect.stack()[0][3])

    def get_text_encoding(self) -> Optional[str]:
        return None

    def stop(self):
        self.interrupted = True
