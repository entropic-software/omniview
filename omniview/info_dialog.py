from pathlib import Path
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

from omniview.common import *
from omniview.ui_info_dialog import Ui_InfoDialog


class InfoDialog(QDialog):
    def __init__(self, parent=None, filename: str = ""):
        super().__init__(parent)
        self.ui = Ui_InfoDialog()
        self.ui.setupUi(self)
        self.ui.but_info_close.clicked.connect(self.accept)

        self.ui.text_filename.setText(parent.current_filename)
        self.ui.text_mimetype.setText(parent.mime_type)
        self.ui.text_encoding.setText(parent.text_encoding)
        self.ui.text_compression.setText(parent.compression)
        if parent.current_filename:
            self.ui.text_filesize.setText(
                format_size(Path(parent.current_filename).stat().st_size)
            )
